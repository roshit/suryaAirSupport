// custom collapse navigation
(function() {
    var triggerBttn = document.getElementById('trigger-overlay'),
        overlay = document.querySelector('div.overlay'),
        closeBttn = overlay.querySelector('button.overlay-close');
        // links = overlay.querySelector('a.overlay-link');

    transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        },
        transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
        support = {
            transitions: Modernizr.csstransitions
        };

    function toggleOverlay() {
        if (classie.has(overlay, 'open')) {
            classie.remove(overlay, 'open');
            classie.add(overlay, 'close');
            var onEndTransitionFn = function(ev) {
                if (support.transitions) {
                    if (ev.propertyName !== 'visibility') return;
                    this.removeEventListener(transEndEventName, onEndTransitionFn);
                }
                classie.remove(overlay, 'close');
            };
            if (support.transitions) {
                overlay.addEventListener(transEndEventName, onEndTransitionFn);
            } else {
                onEndTransitionFn();
            }
        } else if (!classie.has(overlay, 'close')) {
            classie.add(overlay, 'open');
        }
    }

    triggerBttn.addEventListener('click', toggleOverlay);
    closeBttn.addEventListener('click', toggleOverlay);
	$('.overlay-list').find('li').each(function() {
	    this.querySelector('a.overlay-link').addEventListener('click', toggleOverlay);
	});
})();

$(document).ready(function() {
	// activities carousel indicator color change
    $('#activities_carousel').on('slide.bs.carousel', function(e) {
        var activeSlider = e.to;

        $('.activities_carousel .carousel-indicators').find('li').each(function() {
            if ($(this).data('slideTo') < activeSlider)
                $(this).find('svg').css({
                    fill: '#FFD558'
                });
            else
                $(this).find('svg').css({
                    fill: '#2C2C2A'
                });
        });
    });

	// activities carousel active switch
    $('#activities_carousel').on('slide.bs.carousel', function() {
        var resetTimeout;
        clearTimeout(resetTimeout);

        var i = 0;
        var activeSlideNumber = 0;
        var carouselIndicatorsDefault = $(this).find('.carousel-indicators-large > li');
        var carouselIndicatorsCustom = $('.carousel-indicators-medium > li');

        resetTimeout = setTimeout(function() {

            for (i = 0; i < carouselIndicatorsDefault.length; i++) {

                if (carouselIndicatorsDefault.eq(i).hasClass('active')) {
                    activeSlideNumber = i;
                }

            }

            carouselIndicatorsCustom.removeClass('active');
            carouselIndicatorsCustom.eq(activeSlideNumber).addClass('active');

        }, 20);
    });

	// changing style of navigation on scroll
    var scrollTop = 0;
    $(window).scroll(function() {
        scrollTop = $(window).scrollTop();
        // $('.counter').html(scrollTop);

        if (scrollTop >= 100) {
            $('#logo').removeClass('brand_image--hide');
            $('#logo-white').addClass('brand_image--hide');
            $('#nav').addClass('nav-scrolled');
            $('#nav').removeClass('navbar-dark');
            $('#nav').addClass('navbar-light');
        } else if (scrollTop < 100) {
			$('#logo').addClass('brand_image--hide');
            $('#logo-white').removeClass('brand_image--hide');
			$('#nav').removeClass('nav-scrolled');
            $('#nav').addClass('navbar-dark');
            $('#nav').removeClass('navbar-light');
        }

    });

	$('body').scrollspy({
		target: '#nav',
		offset: 79
	});

	$('.top-navigation a').click(function() {

        var target = $(this.hash);
        if (target) {
          $('html,body').animate({
            scrollTop: $(target).offset().top -77
          }, 1000);
          return false;
        }
	});

	$('#activities_carousel').carousel({
	  interval: 5000
	})

});

// parallax plane
$(window).scroll(function() {
    var wScroll = $(this).scrollTop();

    $('#about_plane').css({
        // 'transform': 'translateY(-'+ wScroll / 10 +'%)'
        'transform': 'translate(' + wScroll / 40 + '%, -' + wScroll / 10 + '%)'
    });

	if(wScroll > $('#about_title').offset().top - $(window).height()+400){
		$('.about_content--title').addClass('scrolled_title');
		// console.log('done');
	}
	if(wScroll < $('#about_title').offset().top - $(window).height()+400){
		$('.about_content--title').removeClass('scrolled_title');
		// console.log('done');
	}

	if(wScroll > $('#objective_title').offset().top - $(window).height()+400){
		$('#objective_title').addClass('scrolled_title');
		// console.log('done');
	}
	if(wScroll < $('#objective_title').offset().top - $(window).height()+400){
		$('#objective_title').removeClass('scrolled_title');
		// console.log('done');
	}

	if(wScroll > $('#activities_title').offset().top - $(window).height()+400){
		$('#activities_title').addClass('scrolled_title');
		// console.log('done');
	}
	if(wScroll < $('#activities_title').offset().top - $(window).height()+400){
		$('#activities_title').removeClass('scrolled_title');
		// console.log('done');
	}
});
